// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

// use express-session to create in-memory sessions @martin
var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "teamJAHM"
}));

// allow us to use SQLite3 from node.js & connect to a database @martin
var sqlite3 = require('sqlite3').verbose();
//returns a new Database object, and opens the SQLite database 'blod-db.db‘ automatically
var db = new sqlite3.Database('blog-db.db');

// Code for authentication starts from here @martin
// ---------------------------------------------------------------------------------

// require the 'passport' module for authentication
var passport = require('passport');
// use the local authentication strategy
var LocalStrategy = require('passport-local').Strategy;

// define the local authentication strategy
var localStrategy = new LocalStrategy(function (username, password, done) {

    // query the blog database for the supplied username and retrieve all columns
    db.all("SELECT * FROM Users WHERE username = ?", [username], function (err, rows) {

        // if the query returns no rows, then the username was not found in the database
        if (rows.length == 0) {
            return done(null, false, { message: 'Invalid username!' });
        }

        // there should only be 1 row returned by SQL query if the username was found
        var user = rows[0];

        // retrieve the password string from the 'rows' object returned by the query
        var userPasswordInDb = rows[0].password;

        // if the provided password, does not match what is in the database
        if (userPasswordInDb !== password) {
            return done(null, false, { message: 'Invalid password!' });
        };

        // if the above validation has passed, then user is authenticated
        done(null, user);
    });   
});

// method to be called to save the currently logged in username to the session
passport.serializeUser(function (user, done) {
    done(null, user.username);
});

// method to be called to retrieve all data in the database related to the provided username
passport.deserializeUser(function (username, done) {
    
    // query the blog database for the supplied username
    db.all("SELECT * FROM Users WHERE username = ?", [username], function (err, rows) {  
        if (rows.length > 0) {
            user = rows[0];
            done(null, user);
        }
    });
});

// passport should use the 'local strategy' for authentication
passport.use('local', localStrategy);

// initialise passport
app.use(passport.initialize());
// request passport to use sessions to store its data
app.use(passport.session());

// ---------------------------------------------------------------------------------
// code for authentication ends @martin


app.get(['/','/logout'], function (req, res) {
    res.render('home');
});

app.get('/signup', function (req, res) {
    res.render('signup');
});

app.get('/login', function (req, res) {
    res.render('login');
});

app.get('/addPost', function (req, res) {
    res.render('addPost');
});


// Serve files form "/public" folder
app.use(express.static(__dirname + "/public"));

// --------------------------------------------------------------------------

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});